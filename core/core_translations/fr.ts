<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>BuildInfo</name>
    <message>
        <location filename="../buildinfo.cpp" line="11"/>
        <source>Unkown build version</source>
        <translation>Version de build inconnue</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../db.cpp" line="221"/>
        <location filename="../db.cpp" line="232"/>
        <location filename="../db.cpp" line="233"/>
        <location filename="../db.cpp" line="234"/>
        <source>Alliaceae</source>
        <translation>Alliacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="222"/>
        <location filename="../db.cpp" line="235"/>
        <location filename="../db.cpp" line="236"/>
        <location filename="../db.cpp" line="237"/>
        <location filename="../db.cpp" line="238"/>
        <source>Apiaceae</source>
        <translation>Apiacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="223"/>
        <location filename="../db.cpp" line="239"/>
        <location filename="../db.cpp" line="240"/>
        <location filename="../db.cpp" line="241"/>
        <source>Asteraceae</source>
        <translation>Astéracées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="224"/>
        <location filename="../db.cpp" line="242"/>
        <location filename="../db.cpp" line="243"/>
        <location filename="../db.cpp" line="244"/>
        <location filename="../db.cpp" line="245"/>
        <location filename="../db.cpp" line="246"/>
        <location filename="../db.cpp" line="247"/>
        <location filename="../db.cpp" line="248"/>
        <source>Brassicaceae</source>
        <translation>Brassicacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="225"/>
        <location filename="../db.cpp" line="249"/>
        <location filename="../db.cpp" line="250"/>
        <location filename="../db.cpp" line="251"/>
        <source>Chenopodiaceae</source>
        <translation>Chénopodiacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="226"/>
        <location filename="../db.cpp" line="252"/>
        <location filename="../db.cpp" line="253"/>
        <location filename="../db.cpp" line="254"/>
        <location filename="../db.cpp" line="255"/>
        <location filename="../db.cpp" line="256"/>
        <source>Cucurbitaceae</source>
        <translation>Cucurbitacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="227"/>
        <location filename="../db.cpp" line="257"/>
        <location filename="../db.cpp" line="258"/>
        <location filename="../db.cpp" line="259"/>
        <source>Fabaceae</source>
        <translation>Fabacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="228"/>
        <location filename="../db.cpp" line="260"/>
        <location filename="../db.cpp" line="261"/>
        <location filename="../db.cpp" line="262"/>
        <location filename="../db.cpp" line="263"/>
        <source>Solanaceae</source>
        <translation>Solanacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="229"/>
        <location filename="../db.cpp" line="264"/>
        <source>Valerianaceae</source>
        <translation>Valérianacées</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="232"/>
        <source>Garlic</source>
        <translation>Ail</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="233"/>
        <source>Onion</source>
        <translation>Ognon</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="234"/>
        <source>Leek</source>
        <translation>Poireau</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="235"/>
        <source>Carrot</source>
        <translation>Carotte</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="236"/>
        <source>Celery</source>
        <translation>Céleri</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="237"/>
        <source>Fennel</source>
        <translation>Fenouil</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="238"/>
        <source>Parsnip</source>
        <translation>Panais</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="239"/>
        <source>Chicory</source>
        <translation>Chicorée</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="240"/>
        <source>Belgian endive</source>
        <translation>Endive</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="241"/>
        <source>Lettuce</source>
        <translation>Laitue</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="242"/>
        <source>Cabbage</source>
        <translation>Chou pommé</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="243"/>
        <source>Brussel Sprouts</source>
        <translation>Chou de Bruxelles</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="244"/>
        <source>Kohlrabi</source>
        <translation>Chou-rave</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="245"/>
        <source>Cauliflower</source>
        <translation>Chou-fleur</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="246"/>
        <source>Broccoli</source>
        <translation>Brocoli</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="247"/>
        <source>Turnip</source>
        <translation>Navet</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="248"/>
        <source>Radish</source>
        <translation>Radis rose</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="249"/>
        <source>Beetroot</source>
        <translation>Betterave</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="250"/>
        <source>Chard</source>
        <translation>Blette</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="251"/>
        <source>Spinach</source>
        <translation>Épinard</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="252"/>
        <source>Cucumber</source>
        <translation>Concombre</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="253"/>
        <source>Zucchini</source>
        <translation>Courgette</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="254"/>
        <source>Melon</source>
        <translation>Melon</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="255"/>
        <source>Watermelon</source>
        <translation>Pastèque</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="256"/>
        <source>Winter squash</source>
        <translation>Courge</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="257"/>
        <source>Bean</source>
        <translation>Haricot</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="258"/>
        <source>Fava bean</source>
        <translation>Fève</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="259"/>
        <source>Pea</source>
        <translation>Pois</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="260"/>
        <source>Eggplant</source>
        <translation>Aubergine</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="261"/>
        <source>Pepper</source>
        <translation>Poivron</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="262"/>
        <source>Potatoe</source>
        <translation>Pomme de terre</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="263"/>
        <source>Tomato</source>
        <translation>Tomate</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="264"/>
        <source>Mâche</source>
        <translation>Mâche</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="266"/>
        <source>Cultivation and Tillage</source>
        <translation>Travail du sol</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="267"/>
        <source>Fertilize and Amend</source>
        <translation>Fertilisation et amendement</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="268"/>
        <source>Irrigate</source>
        <translation>Irrigation</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="269"/>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="270"/>
        <source>Pest and Disease</source>
        <translation>Parasites et maladies</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="271"/>
        <source>Prune</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="272"/>
        <source>Row Cover and Mulch</source>
        <translation>Bâches</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="273"/>
        <source>Stale Bed</source>
        <translation>Faux semis</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="274"/>
        <source>Thin</source>
        <translation>Éclaircissage</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="275"/>
        <source>Trellis</source>
        <translation>Palissage</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="276"/>
        <source>Weed</source>
        <translation>Désherbage</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="278"/>
        <source>kilogram</source>
        <translation>kilogramme</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="278"/>
        <source>kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="279"/>
        <source>bunch</source>
        <translation>botte</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="279"/>
        <source>bn</source>
        <translation>bte</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="280"/>
        <source>head</source>
        <translation>pièce</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="280"/>
        <source>hd</source>
        <translation>pc</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="283"/>
        <source>Unknown company</source>
        <translation>Fournisseur indéfini</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="304"/>
        <source>Direct sow</source>
        <translation>Semis direct</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="305"/>
        <source>Greenhouse sow</source>
        <translation>Semis en pépinière</translation>
    </message>
    <message>
        <location filename="../db.cpp" line="306"/>
        <source>Transplant</source>
        <translation>Plantation</translation>
    </message>
    <message>
        <source>Transplant sow</source>
        <translation type="vanished">Plantation</translation>
    </message>
</context>
<context>
    <name>MDate</name>
    <message>
        <location filename="../mdate.cpp" line="202"/>
        <source>Winter</source>
        <translation>Hiver</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="204"/>
        <source>Spring</source>
        <translation>Printemps</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="206"/>
        <source>Summer</source>
        <translation>Été</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="208"/>
        <source>Fall</source>
        <translation>Automne</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="264"/>
        <source>today</source>
        <translation>aujourd&apos;hui</translation>
    </message>
    <message>
        <location filename="../mdate.cpp" line="271"/>
        <source>today</source>
        <comment>abbreviation</comment>
        <translation>auj.</translation>
    </message>
</context>
<context>
    <name>Planting</name>
    <message>
        <location filename="../planting.cpp" line="920"/>
        <source>Unknown company</source>
        <translation>Fournisseur indéfini</translation>
    </message>
    <message>
        <location filename="../planting.cpp" line="926"/>
        <source>Unkown company</source>
        <translation>Fournisseur indéfini</translation>
    </message>
    <message>
        <location filename="../planting.cpp" line="1076"/>
        <source>beds</source>
        <translation>planches</translation>
    </message>
    <message>
        <location filename="../planting.cpp" line="1076"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../planting.cpp" line="1080"/>
        <source>%1, %2 (%L3/%L4 %5 assigned)</source>
        <translation>%1, %2 (%L3/%L4 %5 affectés)</translation>
    </message>
    <message>
        <location filename="../planting.cpp" line="1088"/>
        <source>%1, %2 (%L3/%L4 %5 to assign)</source>
        <translation>%1, %2 (%L3/%L4 %5 à affecter)</translation>
    </message>
</context>
<context>
    <name>Print</name>
    <message>
        <location filename="../print.cpp" line="75"/>
        <source>General crop plan</source>
        <translation>Plane de culture général</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="89"/>
        <location filename="../print.cpp" line="129"/>
        <location filename="../print.cpp" line="167"/>
        <location filename="../print.cpp" line="206"/>
        <location filename="../print.cpp" line="366"/>
        <location filename="../print.cpp" line="414"/>
        <source>Crop</source>
        <translation>Espèce</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="90"/>
        <location filename="../print.cpp" line="130"/>
        <location filename="../print.cpp" line="168"/>
        <location filename="../print.cpp" line="207"/>
        <location filename="../print.cpp" line="367"/>
        <location filename="../print.cpp" line="415"/>
        <source>Variety</source>
        <translation>Variété</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="91"/>
        <location filename="../print.cpp" line="128"/>
        <location filename="../print.cpp" line="166"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="92"/>
        <location filename="../print.cpp" line="131"/>
        <location filename="../print.cpp" line="205"/>
        <source>TP</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="93"/>
        <location filename="../print.cpp" line="169"/>
        <location filename="../print.cpp" line="208"/>
        <source>FH</source>
        <translation>DR</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="94"/>
        <source>LH</source>
        <translation>FR</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="95"/>
        <location filename="../print.cpp" line="170"/>
        <location filename="../print.cpp" line="209"/>
        <source>Length</source>
        <translation>Lgr.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="96"/>
        <location filename="../print.cpp" line="172"/>
        <location filename="../print.cpp" line="211"/>
        <source>Rows</source>
        <translation>Rangs</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="97"/>
        <source>Spac.</source>
        <translation>Dist.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="98"/>
        <location filename="../print.cpp" line="174"/>
        <location filename="../print.cpp" line="214"/>
        <location filename="../print.cpp" line="243"/>
        <location filename="../print.cpp" line="269"/>
        <source>Locations</source>
        <translation>Emplacements</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="115"/>
        <source>Greenhouse crop plan</source>
        <translation>Plan de culture pépinière</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="132"/>
        <location filename="../print.cpp" line="212"/>
        <source># trays</source>
        <translation>Nb. plaques</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="133"/>
        <location filename="../print.cpp" line="213"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="134"/>
        <source>Seeds/hole</source>
        <translation>Graines/trou</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="135"/>
        <source># seeds</source>
        <translation>Nb. graines</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="136"/>
        <source>Seed weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="153"/>
        <source>Field sowing crop plan</source>
        <translation>Plan de culture semis direct</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="171"/>
        <location filename="../print.cpp" line="210"/>
        <source>Spacing</source>
        <translation>Dist.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="173"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="191"/>
        <source>Field transplanting crop plan</source>
        <translation>Plan de culture plantation</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="231"/>
        <source>Task calendar</source>
        <translation>Calendrier des tâches</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="241"/>
        <location filename="../print.cpp" line="267"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="242"/>
        <location filename="../print.cpp" line="268"/>
        <source>Planting</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="244"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="245"/>
        <source>Tags</source>
        <translation>Étiquettes</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="246"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="258"/>
        <source>Harvests</source>
        <translation>Récoltes</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="270"/>
        <location filename="../print.cpp" line="370"/>
        <source>Quantity</source>
        <translation>Quantité</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="271"/>
        <source>Labor time</source>
        <translation>Temps de travail</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="375"/>
        <source>Monthly Seed List (%1)</source>
        <translation>Liste des semences par mois (%1)</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="378"/>
        <source>Quarterly Seed List (%1)</source>
        <translation>Liste des semences par trimestre (%1)</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="381"/>
        <source>Yearly Seed List (%1)</source>
        <translation>Liste des semences (%1)</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="413"/>
        <source>Transplanting date</source>
        <translation>Date plantation</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="419"/>
        <source>Transplant List (%1)</source>
        <translation>Liste des plants (%1)</translation>
    </message>
    <message>
        <source>Seed list</source>
        <translation type="vanished">Liste des semences</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="368"/>
        <location filename="../print.cpp" line="416"/>
        <source>Company</source>
        <translation>Fournisseur</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="369"/>
        <location filename="../print.cpp" line="417"/>
        <source>Number</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>Transplant list</source>
        <translation type="vanished">Liste des plants</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="594"/>
        <source> W%1, %2 − %3</source>
        <translation> S%1, %2 − %3</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="656"/>
        <source>%1 beds</source>
        <translation>%1 pl.</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="658"/>
        <source>%1 bed m.</source>
        <translation>%1 m pl.</translation>
    </message>
    <message>
        <source>%1, %2 rows x %3 cm</source>
        <translation type="vanished">%1, %2 rangs x %3 cm</translation>
    </message>
    <message>
        <source>%1 x %2, %3 seeds per hole</source>
        <translation type="vanished">%1 x %2, %3 graines par trou</translation>
    </message>
    <message>
        <location filename="../print.cpp" line="818"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
</context>
<context>
    <name>Task</name>
    <message>
        <location filename="../task.cpp" line="87"/>
        <source>%L1 x %L2, %L3 seeds [%4]</source>
        <translation>%L1 x %L2, %L3 graines [%4]</translation>
    </message>
    <message>
        <location filename="../task.cpp" line="92"/>
        <source>%L1 x %L2 [%4]</source>
        <translation>%L1 x %L2 [%4]</translation>
    </message>
    <message>
        <location filename="../task.cpp" line="103"/>
        <source>%L1 beds</source>
        <translation>%L1 pl.</translation>
    </message>
    <message>
        <location filename="../task.cpp" line="105"/>
        <source>%L1 bed m.</source>
        <translation>%L1 m pl.</translation>
    </message>
    <message>
        <location filename="../task.cpp" line="107"/>
        <source>%L1, %L2 rows x %L3 cm</source>
        <translation>%L1, %L2 rangs x %L3 cm</translation>
    </message>
    <message>
        <source>%L1 x %L2, %L3 seeds per hole</source>
        <translation type="vanished">%L1 x %L2, %L3 graines par trou</translation>
    </message>
    <message>
        <source>%L1 x %L2</source>
        <translation type="vanished">%L1 x %L2</translation>
    </message>
</context>
<context>
    <name>Variety</name>
    <message>
        <location filename="../variety.cpp" line="69"/>
        <source>Unknown</source>
        <translation>Inconnue</translation>
    </message>
</context>
</TS>
